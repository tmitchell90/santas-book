<?php

/**
 * 
 * Digitickets Xmas Mini App
 * 
 * */

require __DIR__ . '/vendor/autoload.php';

// Set basic App config
static $appConfig = [
    'baseURL' => 'https://api.digitickets.co.uk/v2/',
    'apiPaths' => [
      'login' => 'users/auth',
      'getOrder' => 'orders/',
      'getCats' => 'categories/'
    ]
];

// Start the session
session_start();

// Set user details variables in the session if not already
if (!isset($_SESSION['userDetails'])) {
  $_SESSION['userDetails'] = [
    'apiKey' => null,
    'grotto' => null,
    'username' => null,
    'error' => false,
    'errorMessage' => null
  ];
}

// Standard Curl request 
function sendRequest(array $post = null, string $endpoint, array $appConfig){

    $ch = curl_init($appConfig['baseURL'] . $endpoint);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    if ($post){
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    } 
    
    // execute!
    $response = curl_exec($ch);
    
    // close the connection, release resources used
    curl_close($ch);
  
    return $response;
}

// Send a login request to DT and then set user details in session
function login(array $appConfig, string $username, string $password) {
  if (!isset($_SESSION['userDetails']['apiKey']) || $_SESSION['userDetails']['apiKey'] == null){
    
    $response = sendRequest([
          'username' => $username,
          'password' => $password
      ], 
      $appConfig['apiPaths']['login'],
      $appConfig
    );
  $response = json_decode($response);

    if (isset($response->success) && $response->success === true) {

      setUserDetails($response->user->apiKey, $response->user->username);
      clearErrors();
       
    } else {

      setError($response->message);

    }
  }
  
  return $_SESSION;
}

// Setting error message
function setError(string $message){
  $_SESSION['userDetails']['error'] = true;
  $_SESSION['userDetails']['errorMessage'] = $message;
}

function clearErrors(){
  $_SESSION['userDetails']['error'] = null;
  $_SESSION['userDetails']['errorMessage'] = null;
}

// Set user details in session
function setUserDetails(string $apiKey, string $username){
  $_SESSION['userDetails']['apiKey'] = $apiKey;
  $_SESSION['userDetails']['username'] = $username;
}

// Set user details in session
function setGrottoNumber(int $number){
  $_SESSION['userDetails']['grotto'] = $number;
}

function getOrderDetails(array $appConfig, string $bookingRef, string $apiKey){
  return sendRequest(
    null,
    $appConfig['apiPaths']['getOrder'] . $bookingRef . '?apiKey=' . $apiKey . '&resolve=orderitems.fielddata.fields',
    $appConfig
  );
}

// check session for apiKey and username
function isloggedin(array $appConfig){

// Check if we have session details set
if (isset($_SESSION['userDetails']['apiKey']) && ($_SESSION['userDetails']['apiKey'] != null)) {
	return true;
}
	return false;
}

function logout(bool $returnstatus){
	session_unset();
  session_destroy();
  if ($returnstatus === true) {
    echo json_encode($_SESSION);
  }
}


// Login request 
if(isset($_REQUEST['login'])){
	
    header('Content-type: application/json');

    //check if logged in already
    if (isloggedin($appConfig)) {
    	echo json_encode($_SESSION);
    	exit;
    }

    //check for empty, null or non existant strings 
    if (isset($_POST['username']) 
    	&& ($_POST['username'] === null || $_POST['username'] === "")
    	&& isset($_POST['password']) 
    	&& ($_POST['password'] === null || $_POST['password'] === "")) {
    	setError('Please provide a username and password...');
    	echo json_encode($_SESSION);
    	exit;
    }

    // if all good then attempt to log user in
    login($appConfig, $_POST['username'], $_POST['password']);
    echo json_encode($_SESSION);
    exit;
	
}

// Logout request 
if(isset($_REQUEST['logout'])){
    logout(true);
}

// Logout request 
if(isset($_REQUEST['updategrotto'])){

  header('Content-type: application/json');

  if (isset($_POST['number']) && is_numeric($_POST['number'])) {
    setGrottoNumber(intval($_POST['number']));
  } else {
    setError('Sorry we cannot recognise that grotto number...');
  }

  echo json_encode($_SESSION);
  exit;

}

// Request to get login status 
if(isset($_REQUEST['loginstatus'])){
	
    header('Content-type: application/json');
    //check if logged in already
    if (isloggedin($appConfig)) {
    	echo json_encode($_SESSION);
    	exit;
    }
    echo json_encode(false);
    exit;
	
}

// Booking detail request
if(isset($_REQUEST['sendBookingToSanta'])){
	
    header('Content-type: application/json');

    if (!isset($_POST['bookingref']) || $_POST['bookingref'] === '') {
    	echo json_encode(['status' => false, 'message' => 'Please enter a booking reference...']);
    	exit;
    }

    if ($_SESSION['userDetails']['apiKey'] === null) {
      echo json_encode(['status' => false, 'message' => "Somebody else logged in to your account, please log in again...", 'logout' => true]);
      exit;
    }

    $order = getOrderDetails($appConfig, $_POST['bookingref'], $_SESSION['userDetails']['apiKey']);
    $order = json_decode($order);

    if (isset($order->error)) {
      $message = $order->error;
      $logout = false;
      if ($order->error == "Your API key was not found") {
        $message = "Somebody else logged in to your account, please log in again...";
        $logout = true;
        logout(false);
      }
      echo json_encode(['status' => false, 'message' => $message, 'logout' => $logout]);
      exit;
    }

    $items = [];
    if ($order->orderitems) {
      foreach ($order->orderitems as $item) {
        $listitem = [
            'name' => $item->name,
            'instances' => [],
            'qty' => $item->qty,
            'datetime' => date("D M j G:i", strtotime($item->eventTime))
        ];
        for($i = 0; $i <= ($item->qty - 1); $i++){
          $iteminstance = [];
          foreach ($item->fielddata as $fielddata) {
            if ($fielddata->itemInstance == $i) {
              $record = [
                'question' => $fielddata->fields->name,
                'answer' => $fielddata->value
              ];
              array_push($iteminstance, $record);
            }
          }
          if (!empty($iteminstance)) {
            array_push($listitem['instances'], $iteminstance);
          }
        }
        array_push($items, $listitem);
      } 
    }

      $options = array(
      'cluster' => 'eu',
      'useTLS' => true
      );

      $pusher = new Pusher\Pusher(
        'd886890018b345570a5d',
        '324a12b5d2d605dc4bb2',
        '1165054',
        $options
      );

    $data = array(
      'grotto' => $_SESSION['userDetails']['grotto'],
      'bookingRef' => $order->bookingRef,
      'orderitems' => $items
      );

    $pusher->trigger('my-channel', 'customer-entered', $data);

    echo json_encode(['status' => true, 'order' => $order]);
    exit;
	
}


?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Willow Farm Santa App</title>
  <meta name="description" content="An application created for staff members of Willows Farm">
  <meta name="author" content="DigiTickets">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;700&family=Potta+One&family=Satisfy&display=swap" rel="stylesheet">
  <script src="https://js.pusher.com/7.0/pusher.min.js"></script>

<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">

<script type="text/javascript">

    function makeRequest (method, url, data) {
    return new Promise(function (resolve, reject) {
      var xhr = new XMLHttpRequest();
      xhr.open(method, url);
      xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
      xhr.onload = function () {
        if (this.status >= 200 && this.status < 300) {
          resolve(xhr.response);
        } else {
          reject({
            status: this.status,
            statusText: xhr.statusText
          });
        }
      };
      xhr.onerror = function () {
        reject({
          status: this.status,
          statusText: xhr.statusText
        });
      };
      if(method=="POST" && data){
          xhr.send(data);
      }else{
          xhr.send();
      }
    });
  }

  function setStage(stageName){
    let htmlCollection = document.getElementsByClassName("stage");
    let array = [].slice.call(htmlCollection)
      for (let i = 0; i < array.length; i++) {
          array[i].style.display = 'none';
          if(array[i].getAttribute('data-stage') === stageName){
            array[i].style.display = 'block';
            if (stageName !== 'login') {
              showStatusBar(true);
            } else {
              showStatusBar(false);
            }
          }
      }
  }

  function showStatusBar(show){
    let statusBar = document.getElementById('statusbar');
    if (show === true) {
        statusBar.classList.add('active');
    } else {
       statusBar.classList.remove('active');
    }
  }

  function isLoggedIn() {
    makeRequest('GET', "?loginstatus").then(function(data){
            var result=JSON.parse(data);
            if (result !== false) {
              if (result.userDetails.grotto !== null) {
                setStage('device');
                updateGrottoNumber(result.userDetails.grotto);
              } else {
                setStage('grotto');
              }
              showStatusBar(true);
            } else {
              setStage('login');
            }
      }
    )
  }

  isLoggedIn();
</script>

  <link rel="stylesheet" href="styling/style.css">
</head>

<body id="body" class='body'>

<!-- Login Screen -->
  <div class='stage' data-stage="login" id="login">    
  	<img src="images/logo.png">
  	<input autocomplete="off" id="username" type="text" placeholder="Your username...">
	  <input autocomplete="off" id="password" type="password" placeholder="Your password...">
	  <button id="login__button">Log in</button>
</div>
<!---->


<!-- Grotto Select Screen -->
<div class='stage' data-stage="grotto" style="display:none;"id="grottoselect">
  <h2>Which Grotto are you working in?</h2>
  <ul>
    <li>
      <a class="updategrotto" data-grotto='1'>One</a>
    </li>
        <li>
      <a class="updategrotto" data-grotto='2'>Two</a>
    </li>
        <li>
      <a class="updategrotto" data-grotto='3'>Three</a>
    </li>
        <li>
      <a class="updategrotto" data-grotto='4'>Four</a>
    </li>
        <li>
      <a class="updategrotto" data-grotto='5'>Five</a>
    </li>
        <li>
      <a class="updategrotto" data-grotto='6'>Six</a>
    </li>
        <li>
      <a class="updategrotto" data-grotto='7'>Seven</a>
    </li>
        <li>
      <a class="updategrotto" data-grotto='8'>Eight</a>
    </li>
  </ul>
</div>  
<!---->

<!-- Device Type Screen -->
<div class='stage' data-stage="device" id="deviceselect">
  <h2>What is this device being used for?</h2>
  <ul>
      <li>
        <a class="devicetype" data-type="search">
          <img src="images/scan-icon.png">
          <em>
            Scanning entry
          </em>
        </a>
    </li>
    <li>
      <a class="devicetype" data-type="book">
        <img src="images/book-icon.png">
        <em> 
          Santas Book
        </em>
    </a>
    </li>
  </ul>
</div>
<!---->

<!-- Order Search Screen -->
<div class='stage' data-stage="search" id="search">
		<h2>Send a booking to Santa......</h2>
  		<input autocomplete="off" id="bookingref" type="text" placeholder="Scan in their booking ref...">
  		<button id="search__button">Send now</button>
</div>
<!---->

<!-- Success sent to santa Screen -->
<div  class='stage' data-stage="success" style="display:none;"  id="sentsuccess">
  <img src="images/tick.png">
    <h2>All sent to Santa!</h2>
    <button onclick="setStage('search')">Scan another ticket</button>
</div>  
<!---->

<!-- Book Results Screen -->
<div  class='stage' data-stage="book" id='book'>
  <div id="noresultsmessage">
    <img src="images/b-scan-icon.png">
    <h2>Start scanning your customers tickets for their details to start appearing here!</h2>
  </div>
  <ul id="resultlist">
  </ul>
</div>
<!---->

<!-- Loading Screen -->
<div style="display:none;" id="loading">
		<img src="images/snowflake.png">   
</div>
<!---->

<div id="statusbar">
  <button id="changesetup">Change setup</button>
  <span id="grottoref" data-grotto=""></span>
  <button id="logout" class="negative">Logout</button>
</div>
<!---->


<!-- Error message panel -->
<div class='errorbar' id="errorbar">
	Error message goes here!
</div>
<!---->

<script type="text/javascript">
	
	const loading = document.getElementById('loading'),
	errorBar = document.getElementById('errorbar'),
	body = document.getElementById('body');

  var pusher = null;

	function showLoading(){
		loading.style.display = 'block';
		setTimeout( function() {
			loading.classList.add('active')
		}, 10);
	}

	function hideLoading(){
		loading.classList.remove('active')
		setTimeout( function() {
			loading.style.display = 'none';
		}, 100);
	}

	function showError(messageText){
		errorbar.innerHTML = messageText;
		errorbar.classList.add('active');
		setTimeout( function() {
			errorbar.classList.remove('active')
		}, 3000);
	}

	document.getElementById('login__button').onclick = function() {
        showLoading();

        let formData = 'username=' + document.getElementById('username').value + '&password=' + document.getElementById('password').value;

        makeRequest('POST', "?login", formData).then(function(data){
              var results=JSON.parse(data);
              hideLoading();
              if (results.userDetails.error === true) {
              	showError(results.userDetails.errorMessage);
              } else {
                showStatusBar(true);
              	setStage('grotto');
              }
    		}
    	)
    };


  // Listen for the grotto change requests
    document.addEventListener('click', function (event) {
      if (!event.target.closest('.updategrotto')) return;
      showLoading();
      let grottonumber = event.target.getAttribute('data-grotto');
      let formData = 'number=' + grottonumber;
      makeRequest('POST', "?updategrotto", formData).then(function(data){
                var results=JSON.parse(data);
                hideLoading();
                if (results.userDetails.error === true) {
                  showError(results.userDetails.errorMessage);
                } else {
                  updateGrottoNumber(grottonumber);
                  setStage('device');
                }
          })
    });

      // Listen for the device change requests
    document.addEventListener('click', function (event) {
      if (!event.target.closest('.devicetype')) return;
      let devicetype = event.target.closest('.devicetype').getAttribute('data-type');
      if (devicetype === 'book') {
        startWatchingForOrders();
      }
      setStage(devicetype);
      resetOrderList();
    });

          // Listen for the remove ticket change requests
    document.addEventListener('click', function (event) {
      if (!event.target.closest('.removeticketinbook')) return;
      showLoading();
      setTimeout(function(){
        event.target.closest('.removeticketinbook').parentNode.parentNode.remove();
        if (document.getElementById('resultlist').innerHTML.trim().length == 0) {
          resetOrderList();
        }
        hideLoading();
      }, 1500)
    });

    function resetOrderList(){
      document.getElementById('noresultsmessage').style.display = 'block';
      document.getElementById('resultlist').style.display = 'none';
      document.getElementById('resultlist').innerHTML = '';
    }

    function updateGrottoNumber(number){
      if (number !== null) {
        document.getElementById('grottoref').innerHTML = 'Grotto number ' + number;
        document.getElementById('grottoref').setAttribute('data-grotto', number);
      } else {
        document.getElementById('grottoref').innerHTML = '';
        document.getElementById('grottoref').setAttribute('data-grotto', '');
      }
    }

    document.getElementById('logout').onclick = function() {
        makeRequest('POST', "?logout", null).then(function(data){
                setStage('login');
                updateGrottoNumber(null);
          })
    };

    document.getElementById('changesetup').onclick = function() {
        setStage('grotto');
        updateGrottoNumber(null);
    };

    document.getElementById('search__button').onclick = function() {
        showLoading();

        let formData = 'bookingref=' + document.getElementById('bookingref').value;

        makeRequest('POST', "?sendBookingToSanta", formData).then(function(data){
              var results=JSON.parse(data);
              hideLoading();
              if (results.status === false) {
              	showError(results.message);
                if (results.logout === true) {
                  setStage('login');
                }
              } else if (results.status === true) {
              	setStage('success');
              }
    		}
    	)
    };

    function getGrottoNumber(){
        return document.getElementById('grottoref').getAttribute('data-grotto');
    }

    function startWatchingForOrders(){

      if (pusher === null) {
          pusher = new Pusher('d886890018b345570a5d', {
              cluster: 'eu'
            });
            var channel = pusher.subscribe('my-channel');
            channel.bind('customer-entered', function(data) {
              if (data.grotto == getGrottoNumber()){
                document.getElementById('noresultsmessage').style.display = 'none';
                document.getElementById('resultlist').style.display = 'block';
                var listItem = '<li><span>TicketRef: ' + data.bookingRef + ' <button class="removeticketinbook">Remove</button></span>';
                for (var i = 0; i < data.orderitems.length; i++) {
                    if (data.orderitems[i].instances.length > 0) {
                       listItem += '<h2> <strong>' + data.orderitems[i].datetime + '</strong> - ' + data.orderitems[i].qty + ' X ' + data.orderitems[i].name + '</h2>';
                       console.log(data);
                        for (var index = 0; index < data.orderitems[i].instances.length; index++) {
                          listItem += '<table><thead>';
                            for (var questionindex = 0; questionindex < data.orderitems[i].instances[index].length; questionindex++) {
                              listItem += '<tr><th>' + data.orderitems[i].instances[index][questionindex].question + '</th><th>' + data.orderitems[i].instances[index][questionindex].answer + '</th></tr>';
                            }
                          listItem += '</thead></table>';
                        };
                    }
                }
                listItem += '</li>';
                document.getElementById('resultlist').innerHTML += listItem; 
              }
             });
      }

    }

</script>


</body>
</html>



